
#include <Wire.h>
#include <SeeedOLED.h>
#include <EEPROM.h>
#include <TimerOne.h>
#include "Arduino.h"
#include <Servo.h>


#define ButtonPin       2
#define EncoderPin1     3
#define EncoderPin2     4
#define ServoPin        5
#define RelayPin        6
#define WaterflowPin    7

#define NoWaterTimeOut  3        // 3s
#define OneSecond       1000
#define dataUpdateInterval  60000  // 60S
#define RelayOn         HIGH
#define RelayOff        LOW

int minimum[] = {255, 255, 255, 255};
int maximum[] = {0, 0, 0, 0};
int sample[] = {0, 0, 0, 0};
int target[] = {50, 50, 50, 50};
int enabled[] = {0, 0, 0, 0};
int analogPin[] {A0 , A1 , A2 , A3};
unsigned long startTime   = 0;
int noWaterCount = 0;
int plant = 0;
int menuSelection = 0;
unsigned char EEPROMAddress = 0;
char buffer[30];
const char* errorMessage = "No Error";

float volumen = 0;

unsigned char buttonFlag  = 0;
unsigned char encoderFlag = 0;
unsigned char updateFlag = 1;

unsigned char waterflowFlag = 0;
unsigned int  waterflowRate = 0;  // L/Hour
unsigned int  nbTopsFan     = 0;  // count the edges

enum WarningStatus
{
  NoWarning          = 0,
  NoWaterWarning     = 1,
  NoSensorWarning     = 1,
};
typedef enum WarningStatus WarningStatusType;
WarningStatusType systemWarning;

enum Status
{
  Standby  =  0,
  Warning  =  1,
  Menu = 2,
  Watering =  3,
  TargetHumiditySetting = 4,
  Calibration = 5,
};
typedef enum Status Systemstatus;
Systemstatus workingStatus;


enum EncoderDir
{
  Anticlockwise = 0,
  Clockwise     = 1,
};

typedef enum EncoderDir EncodedirStatus;
EncodedirStatus encoderRoateDir;

Servo myservo;

void setup() {
  Serial.begin(9600);
  Serial.println("----- Initialize systems ...   -----");
  
  /* Init servo */
  myservo.attach(ServoPin);
  myservo.detach();
  Serial.println("servo initialized ( detached mode )");

  /* Init OLED */
  Wire.begin();
  SeeedOled.init();  //initialze SEEED OLED display
  DDRB |= 0x21;
  PORTB |= 0x21;
  SeeedOled.clearDisplay();          //clear the screen and set start position to top left corner
  SeeedOled.setNormalDisplay();      //Set display to normal mode (i.e non-inverse mode)
  SeeedOled.setPageMode();           //Set addressing mode to Page Mode
  Serial.println("display initialized ...");


  /* Init button */
  pinMode(ButtonPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(ButtonPin), ButtonClick, CHANGE);
  Serial.println("button initialized ...");

  /* Init encoder */
  pinMode(EncoderPin1, INPUT);
  pinMode(EncoderPin2, INPUT);
  attachInterrupt(digitalPinToInterrupt(EncoderPin1), EncoderRotate, RISING);
  Serial.println("encoder initialized ...");

  /* Init water flow */
  pinMode(WaterflowPin, INPUT);
  Serial.println("water flow sensor initialized ...");
  
  /* Init water pump */
  pinMode(RelayPin, OUTPUT);
  Serial.println("water pump initialized ...");

  /* Init ROM */
  if (EEPROM.read(EEPROMAddress) == 0x67) {
    Serial.print("first start EEPROM ");
    EEPROM.write(EEPROMAddress, 0x66);
    for (int p = 0; p < 4; p++) {
      EEPROM.write(minAddress(p), minimum[p]);
      EEPROM.write(maxAddress(p), maximum[p]);
      EEPROM.write(targetAddress(p), target[p]);
      EEPROM.write(enabledAddress(p), enabled[p]);
    }
  } else {
    Serial.println("read EEPROM ...");
    for (int p = 0; p < 4; p++) {
      minimum[p] = EEPROM.read(minAddress(p));
      maximum[p] = EEPROM.read(maxAddress(p));
      target[p] = EEPROM.read(targetAddress(p));
      enabled[p] = EEPROM.read(enabledAddress(p));
    }
  }
  Serial.println("EEPROM initialized ...");

  /* Init system variables */
  startTime = millis();
  workingStatus = Standby;
  systemWarning = NoWarning;
  Serial.println("----- Initialized all systems! -----");
}

void loop() {
  // Input
  switch (workingStatus) {
    case Standby:

      // read sensors every data update interval second
      if (millis() - startTime > dataUpdateInterval) {
        startTime  = millis();
        updateFlag = 1;
        for (int p = 0; p <= 3; p++) {
          if (enabled[p] == 1) {
            readSensorValues(p);
          }
        }
      }

      // read encoder
      if (encoderFlag == 1) {
        delay(100);
        encoderFlag = 0;
        switch (encoderRoateDir) {
          case Clockwise:
            plant++;
            break;
          case Anticlockwise:
            plant--;
            break;
          default:
            break;
        }
        if (plant > 4) {
          plant = 0;
        }
        if (plant < 0) {
          plant = 4;
        }
        SeeedOled.clearDisplay();
        updateFlag = 1;
      }

      // read button
      if (buttonFlag == 1) {
        delay(100);
        buttonFlag = 0;
        if (plant != 4) {
          workingStatus = Menu;
          SeeedOled.clearDisplay();
          updateFlag = 1;
          menuSelection = 0;
        }
      }
      break;
    case Menu:

      // read encoder
      if (encoderFlag == 1) {
        delay(100);
        encoderFlag = 0;
        updateFlag = 1;
        switch (encoderRoateDir) {
          case Clockwise:
            menuSelection++;
            break;
          case Anticlockwise:
            menuSelection--;
            break;
          default:
            break;
        }
        if (menuSelection > 4) {
          menuSelection = 0;
        }
        if (menuSelection < 0) {
          menuSelection = 4;
        }
      }

      // read button
      if (buttonFlag == 1) {
        delay(100);
        buttonFlag = 0;
        updateFlag = 1;
        SeeedOled.clearDisplay();
        switch ( menuSelection ) {
          case 0:
            // Enable / Disable
            if (enabled[plant] == 0) {
              enabled[plant] = 1;
              EEPROM.update(enabledAddress(plant), enabled[plant]);
            } else {
              enabled[plant] = 0;
              EEPROM.update(enabledAddress(plant), enabled[plant]);
            }
            break;
          case 1:
            // manual watering
            waterPlant();
            break;
          case 2:
            // set target humidity
            workingStatus = TargetHumiditySetting;
            break;
          case 3:
            // Reset humidity sensor boundries
            minimum[plant] = 255;
            EEPROM.update(minAddress(plant), minimum[plant]);
            maximum[plant] = 0;
            EEPROM.update(maxAddress(plant), maximum[plant]);
            workingStatus = Calibration;
            break;
          case 4:
            // Exit Menu
            workingStatus = Standby;
            buttonFlag = 0;
            break;
        }
      }
      break;
    case TargetHumiditySetting:
      if (encoderFlag == 1) {
        delay(100);
        encoderFlag = 0;
        updateFlag = 1;
        switch (encoderRoateDir) {
          case Clockwise:
            target[plant] = target[plant] + 5;
            break;
          case Anticlockwise:
            target[plant] = target[plant] - 5;
            break;
          default:
            break;
        }
        if (target[plant] > 100) {
          target[plant] = 100;
        }
        if (target[plant] < 0) {
          target[plant] = 0;
        }
        EEPROM.update(targetAddress(plant), target[plant]);
      }
      if (buttonFlag == 1) {
        delay(100);
        buttonFlag = 0;
        workingStatus = Standby;
        SeeedOled.clearDisplay();
        updateFlag = 1;
      }
      break;
    case Calibration:
      readSensorValues(plant);
      updateFlag = 1;
      if (buttonFlag == 1) {
        delay(100);
        buttonFlag = 0;
        workingStatus = Standby;
        SeeedOled.clearDisplay();
        updateFlag = 1;
      }
      break;
    case Warning:
      delay(100);
      // Abort by button press
      if (buttonFlag ==  1) {
        buttonFlag = 0;
        systemWarning = NoWarning;
        workingStatus = Standby;
        SeeedOled.clearDisplay();
        SeeedOled.setNormalDisplay();
        updateFlag = 1;
        startTime = millis();
        noWaterCount = 0;
      }
      break;
    case Watering:
      if (digitalRead(WaterflowPin) == 1) {
        if (digitalRead(WaterflowPin) == 1) {
          if (waterflowFlag == 0) {
            waterflowFlag = 1;
            nbTopsFan++;
          }
        }
      } else {
        if (waterflowFlag == 1) {
          waterflowFlag = 0;
        }
      }
      // Why ???
      static char noWaterCount = 0;
      // every second
      if ((millis() - startTime) > OneSecond ) {
        waterflowRate = (nbTopsFan * 60 / 73);
        // check water flow
        if (((float)(nbTopsFan) / 73 / 60) < 0.005 ) {
          // abort after 3 sec no water flow
          if (noWaterCount++ >= NoWaterTimeOut) {
            systemWarning = NoWaterWarning;
            switchToWarning("No Water");
          }
          volumen       += (float)((float)(nbTopsFan) / 73 / 60);
        } else {
          noWaterCount = 0;
          volumen       += (float)((float)(nbTopsFan) / 73 / 60 + 0.005);
        }
        nbTopsFan = 0;

        //TODO mach konfigurierbar
        if (volumen >= 0.1) {
          // Switch to Standby
          volumen = 0;
          workingStatus = Standby;
          WaterPumpOff();
          SeeedOled.clearDisplay();
          SeeedOled.setTextXY(4, 2);
          SeeedOled.putString("Done Watering");
          delay(OneSecond);
          SeeedOled.clearDisplay();
          buttonFlag = 0;
        }
        updateFlag = 1;
        startTime = millis();
      }

      // Abort button
      if (buttonFlag == 1) {
        Serial.println("abort by button");
        delay(100);
        buttonFlag = 0;
        WaterPumpOff();
        updateFlag = 1;
        workingStatus = Standby;
        SeeedOled.clearDisplay();
      }
      break;
    default:
      break;
  }

  // Display
  if (updateFlag == 1) {
    updateFlag = 0;
    switch ( workingStatus ) {
      case Calibration:
      case Standby:
        if (plant == 4) {
          displayAllPlant();
        } else {
          displayPlant();
        }
        break;
      case Menu:
        displayMenu();
        break;
      case TargetHumiditySetting:
        displayHumiditySetting();
        break;
      case Watering:
        displayWatering();
        break;
      case Warning:
        displayWarning();
        break;
    }
  }
  
}

void readSensorValues(int plantNumber) {
  delay(100);
  sample[plantNumber] = analogRead(analogPin[plantNumber]);
  if (sample[plantNumber] < 200 || sample[plantNumber] > 800) {
    systemWarning = NoSensorWarning;
    switchToWarning("Sensor Error");
  } else {
    if (sample[plantNumber] / 4 < minimum[plantNumber]) {
      minimum[plantNumber] = sample[plantNumber] / 4;
      EEPROM.update(minAddress(plantNumber), minimum[plantNumber]);
    }
    if (sample[plantNumber] / 4 > maximum[plantNumber] ) {
      maximum[plantNumber] = sample[plantNumber] / 4;
      EEPROM.update(maxAddress(plantNumber), maximum[plantNumber]);
    }
  }
}

void waterPlant() {
  workingStatus = Watering;
  int nextServoLocation = (60 * plant) + 5;
  myservo.attach(ServoPin);
  myservo.write(nextServoLocation );
  delay(OneSecond);
  myservo.detach();
  WaterPumpOn();
}

void switchToWarning(const char* message) {
  // switch to Warning
  volumen = 0;
  updateFlag = 1;
  WaterPumpOff();
  workingStatus = Warning;
  errorMessage =  message;
  buttonFlag = 0;
}

void displayWarning() {
  SeeedOled.clearDisplay();
  SeeedOled.setInverseDisplay();
  SeeedOled.setTextXY(2, 4);
  SeeedOled.putString("Warning!");
  SeeedOled.setTextXY(5, 3);
  SeeedOled.putString(errorMessage);
}

void displayWatering() {
  SeeedOled.setTextXY(2, 1);
  SeeedOled.putString("A");
  SeeedOled.putNumber(plant);
  SeeedOled.putString(" ");

  sprintf(buffer, "%2d L/H", waterflowRate);
  SeeedOled.setTextXY(3, 4);
  SeeedOled.putString(buffer);

  if ((int)((int)(volumen * 100) % 100) < 10 ) {
    sprintf(buffer, "%2d.0%d L", (int)(volumen), (int)((int)(volumen * 100) % 100));
  } else {
    sprintf(buffer, "%2d.%2d L", (int)(volumen), (int)((int)(volumen * 100) % 100));
  }
  SeeedOled.setTextXY(5, 4);
  SeeedOled.putString(buffer);

}

void displayMenu() {
  SeeedOled.setTextXY(2, 1);
  SeeedOled.putString("Menu A");
  SeeedOled.putNumber(plant);
  SeeedOled.setTextXY(3, 1);
  if (enabled[plant] == 0) {
    SeeedOled.putString("  Enable");
  } else {
    SeeedOled.putString("  Disable");
  }
  SeeedOled.setTextXY(4, 1);
  SeeedOled.putString("  Water Plant");
  SeeedOled.setTextXY(5, 1);
  SeeedOled.putString("  Set Humidity");
  SeeedOled.setTextXY(6, 1);
  SeeedOled.putString("  Reset Sensor");
  SeeedOled.setTextXY(7, 1);
  SeeedOled.putString("  Exit Menu");
  SeeedOled.setTextXY(menuSelection + 3, 1);
  SeeedOled.putString("> ");
}

void displayHumiditySetting() {
  SeeedOled.setTextXY(2, 1);
  SeeedOled.putString("A");
  SeeedOled.putNumber(plant);
  SeeedOled.setTextXY(3, 1);
  SeeedOled.putString("Set Target");
  SeeedOled.setTextXY(4, 1);
  SeeedOled.putString("Humidity");
  SeeedOled.setTextXY(5, 1);
  SeeedOled.putString("tar: ");
  SeeedOled.putNumber(target[plant]);
  SeeedOled.putString(" %");
}


void displayReset () {
  SeeedOled.setTextXY(2, 1);
  SeeedOled.putString("A");
  SeeedOled.putNumber(plant);
  SeeedOled.setTextXY(3, 1);
  SeeedOled.putString("Reset Sensor");
  SeeedOled.setTextXY(4, 1);
  SeeedOled.putString("Now ?");
}

void displayPlant() {
  SeeedOled.setTextXY(2, 1);
  SeeedOled.putString("A");
  SeeedOled.putNumber(plant);
  SeeedOled.putString(" ");
  if (enabled[plant] == 0) {
    SeeedOled.putString("disabled");
  } else {
    SeeedOled.putNumber(minimum[plant]);
    SeeedOled.putString("/");
    SeeedOled.putNumber((int) sample[plant] / 4);
    SeeedOled.putString("/");
    SeeedOled.putNumber(maximum[plant]);
    SeeedOled.putString("  ");
    SeeedOled.setTextXY(4, 1);
    SeeedOled.putString("atm: ");
    SeeedOled.putNumber(humidity(sample[plant], maximum[plant], minimum[plant]));
    SeeedOled.putString(" % ");
    SeeedOled.setTextXY(5, 1);
    SeeedOled.putString("tar: ");
    SeeedOled.putNumber(target[plant]);
    SeeedOled.putString(" % ");
  }

}

void displayAllPlant() {
  SeeedOled.setTextXY(2, 1);
  SeeedOled.putString("   atm -> tar");
  for ( int p = 0 ; p < 4; p++) {
    SeeedOled.setTextXY(p + 3, 1);
    SeeedOled.putString("A");
    SeeedOled.putNumber(p);
    SeeedOled.putString(" ");
    if (enabled[p] == 1) {
      SeeedOled.putNumber(humidity(sample[p], maximum[p], minimum[p]));
      SeeedOled.putString("% -> ");
      SeeedOled.putNumber(target[p]);
      SeeedOled.putString("% ");
    }
  }
}

float humidity(int sample, int maximum, int minimum) {
  return map(sample, minimum * 4, maximum * 4, 100 , 0 );
}

void ButtonClick()
{

  if (digitalRead(ButtonPin) == 0) {
    delay(10);
    if (digitalRead(ButtonPin) == 0) {
      Serial.println("button pressed");
      buttonFlag = 1;
    } else {
      Serial.println("button abrot");
    }
  }
}


void EncoderRotate()
{
  if (digitalRead(EncoderPin1) == 1) {
    delay(10);
    if (digitalRead(EncoderPin1) == 1) {
      if (encoderFlag == 0) {
        encoderFlag = 1;
        if (digitalRead(EncoderPin2) == 1) {
          encoderRoateDir = Clockwise;
        } else {
          encoderRoateDir = Anticlockwise;
        }
      }
    } else {
      Serial.println("encoder abort");
    }
  }
}

void WaterPumpOn()
{
  digitalWrite(RelayPin, RelayOn);
  //dont know why button detects Relay on/off... 
  delay(200);
  buttonFlag = 0; 
  encoderFlag = 0; 
}

void WaterPumpOff()
{
  digitalWrite(RelayPin, RelayOff);
  //dont know why button detects Relay on/off... 
  delay(200);
  buttonFlag = 0; 
  encoderFlag = 0; 
}

int minAddress(int i) {
  return (i * 4) + 1;
}

int maxAddress(int i) {
  return (i * 4) + 2;
}

int targetAddress(int i) {
  return (i * 4) + 3;
}

int enabledAddress(int i) {
  return (i * 4 ) + 4;
}
